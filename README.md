# README #

Koło fortuny - plugin do Wordpress'a, wykorzystujący baze danych z graczami na serwerze Minecraft. Plugin pozwala po zalogowaniu przez gracza (danymi z serwera) na zakręcenie kołem i otrzymanie wylosowanego przedmiotu prosto do ich ekwipunku w grze.

### Krótkie podsumowanie ###

* Koło fortuny dla graczy serwera Minecraft
* v1.1

### Co potrzebne? ###

* Serwer Minecraft
* Serwer MySQL
* Plugin do rejestracji/logowania na serwerze Minecraft wykorzystujący baze danych Authme [Przykład](https://dev.bukkit.org/projects/authme)
* Strona WWW na Wordpress'ie

### Autor? ###

* © Michał 'SospoN' Sobczak